class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get getName() {
        return this.name;
    }

    set changeName(newName) {
        return this.name = newName;
    }

    get getAge() {
        return this.age;
    }
    set changeAge(newAge) {
        return this.age = newAge;
    }
    get getSalary() {
        return this.salary;
    }
    set changeSalary(newSalary) {
        return this.salary = newSalary;
    }
}
class Programmer extends Employee{
    constructor(name,age,salary,lang) {
        super(name,age,salary);
        this.lang=lang;
    }
    get getSalary(){
        return this.salary * 3;
    }

}
const programmer = new Programmer ('Daria',23,20000,'ua');
const programmer2 = new Programmer ('Max',25,30000,'ua');
const programmer3 = new Programmer ('Ira',29,10000,'ua');
const programmer4 = new Programmer ('Katya',20,9000,'ua');
console.log(programmer.getSalary);
console.log(programmer);
console.log(programmer2);
console.log(programmer3);
console.log(programmer4);