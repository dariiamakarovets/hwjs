document.querySelector('.menu-section-amazing-work').addEventListener('click',(event)=> {
    document.querySelector('.item.active').classList.remove('active');
    event.target.classList.add('active');
    let dataItem = event.target.getAttribute("data-tab");
    console.log(dataItem);
    let li=document.querySelectorAll(".picture");
    console.log(li);
li.forEach(elem=>{
    if(elem.getAttribute("data-tab")===dataItem ||dataItem==="All"){
        elem.classList.remove("hidden");
    }
    else {
        elem.classList.add("hidden");
    }

})
})

let works = [
    {
        url:"/gallery/deer-3275594_960_720.webp",
        data:"Graphic Desing"
    },
    {
        url:"/gallery/poly-3295856__340.webp",
        data:"Graphic Desing"
    },
    {
        url:"/gallery/small-poly-3310319_960_720.webp",
        data:"Graphic Desing"
    },
    {
        url:"/gallery/interface-3614766_960_720.webp",
        data:"Graphic Desing"
    },
    {
        url:"/gallery/laptop-2443052_960_720.jpg",
        data:"Web Design"
    },
    {
        url:"/gallery/web-1045994_960_720.jpg",
        data:"Web Design"
    },
    {
        url:"/gallery/istockphoto-984588390-612x612.jpg",
        data:"Landing Pages"
    },
    {
        url:"/gallery/istockphoto-1031211452-612x612.jpg",
        data:"Landing Pages"
    },
    {
        url:"/gallery/istockphoto-1168860252-612x612.jpg",
        data:"Landing Pages"
    },
    {
        url:"/gallery/wordpress-581849_960_720.webp",
        data:"Wordpress"
    },
    {
        url:"/gallery/wordpress-2171594_960_720.jpg",
        data:"Wordpress"
    },
    {
        url:"/gallery/wordpress-3424025_960_720.jpg",
        data:"Wordpress"
    },
]
let button=document.querySelector('.button-load');
button.addEventListener('click',()=>{
    button.remove()
    let container=document.querySelector(".container-with-picture");
    works.forEach(({url, data}) => {
        let li=document.createElement("li");
        li.classList.add("picture");
        li.setAttribute("data-tab",data);
        li.innerHTML=`<img src="${url}" alt="" width="285px" height="206px">`
        container.append(li);
    })
})


