const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];



function filterObj (book){
    if(!book.author){
        throw new Error('Error,no author')
    }
    else

    if(!book.name){
        throw new Error('Error,no name')
    }
    else
    if(!book.price){
        throw new Error('Error,no price')
    }
    else {
    return book
    }
}
function renderBooks (books){
    let div =document.getElementById('root')
    let ul =document.createElement('ul')
    div.append(ul);
    books.forEach(book=>{
        try{
          if(filterObj(book)) {
           let li =document.createElement('li')
              li.textContent=`author: ${book.author};name:${book.name}
                           price:${book.price}`
             ul.append(li);
          }

        }
        catch(e){
            console.log(e)
        }

    })
}
renderBooks(books)